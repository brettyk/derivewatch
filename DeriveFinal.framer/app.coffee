# This imports all the layers for "Screen10-01" into screen1001Layers22
screen1001Layers22 = Framer.Importer.load "imported/Screen10-01"

# This imports all the layers for "Screen9-01" into screen901Layers21
screen901Layers21 = Framer.Importer.load "imported/Screen9-01"

# This imports all the layers for "Screen82-01" into screen8201Layers20
screen8201Layers20 = Framer.Importer.load "imported/Screen82-01"

# This imports all the layers for "Screen81-01" into screen8101Layers19
screen8101Layers19 = Framer.Importer.load "imported/Screen81-01"

# This imports all the layers for "Screen8-01" into screen801Layers18
screen801Layers18 = Framer.Importer.load "imported/Screen8-01"

# This imports all the layers for "Screen7-01" into screen701Layers17
screen701Layers17 = Framer.Importer.load "imported/Screen7-01"

# This imports all the layers for "Screen61-01" into screen6101Layers16
screen6101Layers16 = Framer.Importer.load "imported/Screen61-01"

# This imports all the layers for "Screen6-01" into screen601Layers15
screen601Layers15 = Framer.Importer.load "imported/Screen6-01"

# This imports all the layers for "Screens52-01" into screens5201Layers14
screens5201Layers14 = Framer.Importer.load "imported/Screens52-01"

# This imports all the layers for "Screens51-01" into screens5101Layers13
screens5101Layers13 = Framer.Importer.load "imported/Screens51-01"

# This imports all the layers for "Screens5-01" into screens501Layers12
screens501Layers12 = Framer.Importer.load "imported/Screens5-01"


# This imports all the layers for "Screens41-01" into screens4101Layers10
screens4101Layers10 = Framer.Importer.load "imported/Screens41-01"



# This imports all the layers for "Screens415-01" into screens41501Layers8
screens41501Layers8 = Framer.Importer.load "imported/Screens415-01"

# This imports all the layers for "Screens4-01" into screens401Layers7
screens401Layers7 = Framer.Importer.load "imported/Screens4-01"

# This imports all the layers for "Screens3.2-01" into screens3201Layers6
screens3201Layers6 = Framer.Importer.load "imported/Screens3.2-01"

# This imports all the layers for "Screens3.1-01" into screens3101Layers5
screens3101Layers5 = Framer.Importer.load "imported/Screens3.1-01"


# This imports all the layers for "Screens3-01" into screens301Layers2
screens301Layers2 = Framer.Importer.load "imported/Screens3-01"

# This imports all the layers for "Screens2-01" into screens201Layers1
screens201Layers1 = Framer.Importer.load "imported/Screens2-01"

# This imports all the layers for "BeginScreen-01" into beginscreen01Layers
beginscreen01Layers = Framer.Importer.load "imported/BeginScreen-01"

for layerGroupName of beginscreen01Layers
	console.log(layerGroupName)
	window[layerGroupName] = beginscreen01Layers[layerGroupName]
	
for layerGroupName of screens201Layers1
	console.log(layerGroupName)
	window[layerGroupName] = screens201Layers1[layerGroupName]
# Background1.visible = false
# Curious.visible = false
# StartNew.visible = false


	
for layerGroupName of screens301Layers2
	console.log(layerGroupName)
	window[layerGroupName] = screens301Layers2[layerGroupName]
# 	
# Background03.visible = false
# Bar03.visible = false
	
for layerGroupName of screens3101Layers5
	console.log(layerGroupName)
	window[layerGroupName] = screens3101Layers5[layerGroupName]	
	
# Background31.visible = false
# Cafe31.visible = false

for layerGroupName of screens3201Layers6
	console.log(layerGroupName)
	window[layerGroupName] = screens3201Layers6[layerGroupName]
# Background32.visible = false
# Next32.visible = false
	
for layerGroupName of screens401Layers7
	console.log(layerGroupName)
	window[layerGroupName] = screens401Layers7[layerGroupName]

# Background04.visible = false
# CoffeeBar04.visible = false
	

for layerGroupName of screens41501Layers8
	console.log(layerGroupName)
	window[layerGroupName] = screens41501Layers8[layerGroupName]
# 	
# Background415.visible = false
# Bernies415.visible = false

	
for layerGroupName of screens4101Layers10
	console.log(layerGroupName)
	window[layerGroupName] = screens4101Layers10[layerGroupName]
	
# Background41.visible = false
# Next41.visible = false

for layerGroupName of screens501Layers12
	console.log(layerGroupName)
	window[layerGroupName] = screens501Layers12[layerGroupName]
		
for layerGroupName of screens5101Layers13
	console.log(layerGroupName)
	window[layerGroupName] = screens5101Layers13[layerGroupName]
	
for layerGroupName of screens5201Layers14
	console.log(layerGroupName)
	window[layerGroupName] = screens5201Layers14[layerGroupName]
	
for layerGroupName of screen601Layers15
	console.log(layerGroupName)
	window[layerGroupName] = screen601Layers15[layerGroupName]
	
for layerGroupName of screen6101Layers16
	console.log(layerGroupName)
	window[layerGroupName] = screen6101Layers16[layerGroupName]

for layerGroupName of screen701Layers17
	console.log(layerGroupName)
	window[layerGroupName] = screen701Layers17[layerGroupName]
	
for layerGroupName of screen801Layers18
	console.log(layerGroupName)
	window[layerGroupName] = screen801Layers18[layerGroupName]

for layerGroupName of screen8101Layers19
	console.log(layerGroupName)
	window[layerGroupName] = screen8101Layers19[layerGroupName]

for layerGroupName of screen8201Layers20
	console.log(layerGroupName)
	window[layerGroupName] = screen8201Layers20[layerGroupName]

for layerGroupName of screen901Layers21
	console.log(layerGroupName)
	window[layerGroupName] = screen901Layers21[layerGroupName]

for layerGroupName of screen1001Layers22
	console.log(layerGroupName)
	window[layerGroupName] = screen1001Layers22[layerGroupName]

# Welcome to Framer

# Learn how to prototype: http://framerjs.com/learn
# Drop an image on the device, or import a design from Sketch or Photoshop

Begin.states.add
	second: {opacity:0}
Begin.states.animationOptions = 
	curve: "linear"	
Background.states.add
	second: {opacity:0}
Background.states.animationOptions = 
	curve: "linear"	


Begin.on Events.Click, ->
	Begin.states.next()
	Background.states.next()
	Begin.ignoreEvents = true
	
Curious.states.add
	second: {opacity:0}
Curious.states.animationOptions = 
	curve: "linear"	
	
Curious.on Events.Click, ->
	Curious.states.next()
	Curious.ignoreEvents = true


StartNew.states.add
	second: {opacity:0}
StartNew.states.animationOptions = 
	curve: "linear"	
Background1.states.add
	second: {opacity:0}
Background1.states.animationOptions = 
	curve: "linear"	


StartNew.on Events.Click, ->
	StartNew.states.next()
	Background1.states.next()
	StartNew.ignoreEvents = true
	
	
Bar03.states.add
	second: {opacity:0}
Bar03.states.animationOptions = 
	curve: "linear"	
Background03.states.add
	second: {opacity:0}
Background03.states.animationOptions = 
	curve: "linear"	

Bar03.on Events.Click, ->
	Bar03.states.next()
	Background03.states.next()
	Bar03.ignoreEvents = true
					
Cafe31.states.add
	second: {opacity:0}
Cafe31.states.animationOptions = 
	curve: "linear"	
Background31.states.add
	second: {opacity:0}
Background31.states.animationOptions = 
	curve: "linear"	

Cafe31.on Events.Click, ->
	Cafe31.states.next()
	Background31.states.next()
	Cafe31.ignoreEvents = true

			
			
Next32.states.add
	second: {opacity:0}
Next32.states.animationOptions = 
	curve: "linear"	
Background32.states.add
	second: {opacity:0}
Background32.states.animationOptions = 
	curve: "linear"	


Next32.on Events.Click, ->
	Next32.states.next()
	Background32.states.next()
	Next32.ignoreEvents = true


CoffeeBar04.states.add
	second: {opacity:0}
CoffeeBar04.states.animationOptions = 
	curve: "linear"	
Background04.states.add
	second: {opacity:0}
Background04.states.animationOptions = 
	curve: "linear"	


CoffeeBar04.on Events.Click, ->
	CoffeeBar04.states.next()
	Background04.states.next()
	CoffeeBar04.ignoreEvents = true

Bernies415.states.add
	second: {opacity:0}
Bernies415.states.animationOptions = 
	curve: "linear"	
Background415.states.add
	second: {opacity:0}
Background415.states.animationOptions = 
	curve: "linear"	


Bernies415.on Events.Click, ->
	Bernies415.states.next()
	Background415.states.next()
	Bernies415.ignoreEvents = true


Next41.states.add
	second: {opacity:0}
Next41.states.animationOptions = 
	curve: "linear"	
Background41.states.add
	second: {opacity:0}
Background41.states.animationOptions = 
	curve: "linear"	


Next41.on Events.Click, ->
	Next41.states.next()
	Background41.states.next()
	Next41.ignoreEvents = true
	

Sameen05.states.add
	second: {opacity:0}
Sameen05.states.animationOptions = 
	curve: "linear"	
Background05.states.add
	second: {opacity:0}
Background05.states.animationOptions = 
	curve: "linear"	


Sameen05.on Events.Click, ->
	Sameen05.states.next()
	Background05.states.next()
	Sameen05.ignoreEvents = true
	
VictorLe51.states.add
	second: {opacity:0}
VictorLe51.states.animationOptions = 
	curve: "linear"	
Background51.states.add
	second: {opacity:0}
Background51.states.animationOptions = 
	curve: "linear"	


VictorLe51.on Events.Click, ->
	VictorLe51.states.next()
	Background51.states.next()
	VictorLe51.ignoreEvents = true
			
Next52.states.add
	second: {opacity:0}
Next52.states.animationOptions = 
	curve: "linear"	
Background52.states.add
	second: {opacity:0}
Background52.states.animationOptions = 
	curve: "linear"	


Next52.on Events.Click, ->
	Next52.states.next()
	Background52.states.next()
	Next52.ignoreEvents = true
	
Select06.states.add
	second: {opacity:0}
Select06.states.animationOptions = 
	curve: "linear"	
Background06.states.add
	second: {opacity:0}
Background06.states.animationOptions = 
	curve: "linear"	


Select06.on Events.Click, ->
	Select06.states.next()
	Background06.states.next()
	Select06.ignoreEvents = true
	
Next61.states.add
	second: {opacity:0}
Next61.states.animationOptions = 
	curve: "linear"	
Background61.states.add
	second: {opacity:0}
Background61.states.animationOptions = 
	curve: "linear"	


Next61.on Events.Click, ->
	Next61.states.next()
	Background61.states.next()
	Next61.ignoreEvents = true
			
			
SendInvites07.states.add
	second: {opacity:0}
SendInvites07.states.animationOptions = 
	curve: "linear"	
Background07.states.add
	second: {opacity:0}
Background07.states.animationOptions = 
	curve: "linear"	


SendInvites07.on Events.Click, ->
	SendInvites07.states.next()
	Background07.states.next()
	SendInvites07.ignoreEvents = true		
	
Peaceful08.states.add
	second: {opacity:0}
Peaceful08.states.animationOptions = 
	curve: "linear"	
Background08.states.add
	second: {opacity:0}
Background08.states.animationOptions = 
	curve: "linear"	


Peaceful08.on Events.Click, ->
	Peaceful08.states.next()
	Background08.states.next()
	Peaceful08.ignoreEvents = true
	
Cultural81.states.add
	second: {opacity:0}
Cultural81.states.animationOptions = 
	curve: "linear"	
Background81.states.add
	second: {opacity:0}
Background81.states.animationOptions = 
	curve: "linear"	


Cultural81.on Events.Click, ->
	Cultural81.states.next()
	Background81.states.next()
	Cultural81.ignoreEvents = true	
	
Next82.states.add
	second: {opacity:0}
Next82.states.animationOptions = 
	curve: "linear"	
Background82.states.add
	second: {opacity:0}
Background82.states.animationOptions = 
	curve: "linear"	


Next82.on Events.Click, ->
	Next82.states.next()
	Background82.states.next()
	Next82.ignoreEvents = true	
	
StartAdventure09.states.add
	second: {opacity:0}
StartAdventure09.states.animationOptions = 
	curve: "linear"	
Background09.states.add
	second: {opacity:0}
Background09.states.animationOptions = 
	curve: "linear"	


StartAdventure09.on Events.Click, ->
	StartAdventure09.states.next()
	Background09.states.next()
	StartAdventure09.ignoreEvents = true	

# Define a set of states with names (the original state is 'default')
# Begin.states.add
# 	second: {y:100, scale:0.6, rotationZ:100}
# 	third:  {y:300, scale:1.3, blur:4}
# 	fourth: {y:200, scale:0.9, blur:2, rotationZ:200}
# 
# Set the default animation options
# Begin.states.animationOptions =
# 	curve: "spring(500,12,0)"
# 
# On a click, go to the next state
# Begin.on Events.Click, ->
# 	Begin.states.next()