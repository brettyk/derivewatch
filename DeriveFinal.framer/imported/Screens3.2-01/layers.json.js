window.__imported__ = window.__imported__ || {};
window.__imported__["Screens3.2-01/layers.json.js"] = [
	{
		"id": 43,
		"name": "Background32",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/Background32.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 640,
				"height": 1136
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1453640963"
	},
	{
		"id": 39,
		"name": "Next32",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/Next32.png",
			"frame": {
				"x": 533,
				"y": 74,
				"width": 97,
				"height": 41
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "116189575"
	}
]