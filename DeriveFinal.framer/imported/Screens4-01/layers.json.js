window.__imported__ = window.__imported__ || {};
window.__imported__["Screens4-01/layers.json.js"] = [
	{
		"id": 70,
		"name": "Background04",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/Background04.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 640,
				"height": 1136
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "556023098"
	},
	{
		"id": 49,
		"name": "CoffeeBar04",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/CoffeeBar04.png",
			"frame": {
				"x": 12,
				"y": 658,
				"width": 614,
				"height": 100
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1711189767"
	}
]