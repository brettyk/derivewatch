window.__imported__ = window.__imported__ || {};
window.__imported__["Screens3-01/layers.json.js"] = [
	{
		"id": 39,
		"name": "Background03",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/Background03.png",
			"frame": {
				"x": 1,
				"y": 0,
				"width": 639,
				"height": 1136
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1269497463"
	},
	{
		"id": 35,
		"name": "Bar03",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/Bar03.png",
			"frame": {
				"x": 100,
				"y": 598,
				"width": 150,
				"height": 202
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "55397512"
	}
]