window.__imported__ = window.__imported__ || {};
window.__imported__["Screens415-01/layers.json.js"] = [
	{
		"id": 69,
		"name": "Background415",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/Background415.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 640,
				"height": 1136
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1730034939"
	},
	{
		"id": 26,
		"name": "Bernies415",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/Bernies415.png",
			"frame": {
				"x": 12,
				"y": 277,
				"width": 614,
				"height": 101
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "128369861"
	}
]