window.__imported__ = window.__imported__ || {};
window.__imported__["Screen10-01/layers.json.js"] = [
	{
		"id": 27,
		"name": "Background10",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/Background10.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 640,
				"height": 1136
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "2028190541"
	},
	{
		"id": 29,
		"name": "ShortestPath10",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/ShortestPath10.png",
			"frame": {
				"x": 110,
				"y": 960,
				"width": 421,
				"height": 70
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "2048359055"
	}
]