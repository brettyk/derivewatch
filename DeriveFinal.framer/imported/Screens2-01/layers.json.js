window.__imported__ = window.__imported__ || {};
window.__imported__["Screens2-01/layers.json.js"] = [
	{
		"id": 81,
		"name": "Background1",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/Background1.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 640,
				"height": 1136
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "122218731"
	},
	{
		"id": 77,
		"name": "Curious",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/Curious.png",
			"frame": {
				"x": 212,
				"y": 363,
				"width": 107,
				"height": 68
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "506186687"
	},
	{
		"id": 79,
		"name": "StartNew",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/StartNew.png",
			"frame": {
				"x": 359,
				"y": 362,
				"width": 254,
				"height": 70
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "443940505"
	}
]