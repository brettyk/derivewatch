window.__imported__ = window.__imported__ || {};
window.__imported__["Screens41-01/layers.json.js"] = [
	{
		"id": 71,
		"name": "Background41",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/Background41.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 640,
				"height": 1136
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "2070056933"
	},
	{
		"id": 77,
		"name": "Next41",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/Next41.png",
			"frame": {
				"x": 530,
				"y": 73,
				"width": 98,
				"height": 42
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1229608850"
	}
]