window.__imported__ = window.__imported__ || {};
window.__imported__["Screens3.1-01/layers.json.js"] = [
	{
		"id": 53,
		"name": "Background31",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/Background31.png",
			"frame": {
				"x": 1,
				"y": 0,
				"width": 639,
				"height": 1136
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1450798206"
	},
	{
		"id": 47,
		"name": "Cafe31",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/Cafe31.png",
			"frame": {
				"x": 418,
				"y": 133,
				"width": 150,
				"height": 150
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1450798231"
	}
]