window.__imported__ = window.__imported__ || {};
window.__imported__["Screen8-01/layers.json.js"] = [
	{
		"id": 27,
		"name": "Background08",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/Background08.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 640,
				"height": 1136
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1599838342"
	},
	{
		"id": 29,
		"name": "Peaceful08",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/Peaceful08.png",
			"frame": {
				"x": 374,
				"y": 160,
				"width": 201,
				"height": 258
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "588865180"
	}
]