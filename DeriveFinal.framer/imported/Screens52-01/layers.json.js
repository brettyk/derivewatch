window.__imported__ = window.__imported__ || {};
window.__imported__["Screens52-01/layers.json.js"] = [
	{
		"id": 83,
		"name": "Background52",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/Background52.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 640,
				"height": 1136
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "2050970874"
	},
	{
		"id": 86,
		"name": "Next52",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/Next52.png",
			"frame": {
				"x": 530,
				"y": 75,
				"width": 98,
				"height": 41
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "2050970877"
	}
]