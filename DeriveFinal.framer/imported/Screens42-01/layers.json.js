window.__imported__ = window.__imported__ || {};
window.__imported__["Screens42-01/layers.json.js"] = [
	{
		"id": 74,
		"name": "Background42",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/Background42.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 640,
				"height": 1136
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "195707667"
	},
	{
		"id": 72,
		"name": "Next42",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/Next42.png",
			"frame": {
				"x": 529,
				"y": 73,
				"width": 99,
				"height": 42
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "194992617"
	}
]