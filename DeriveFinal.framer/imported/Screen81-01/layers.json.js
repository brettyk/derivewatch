window.__imported__ = window.__imported__ || {};
window.__imported__["Screen81-01/layers.json.js"] = [
	{
		"id": 28,
		"name": "Background81",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/Background81.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 640,
				"height": 1136
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "297551868"
	},
	{
		"id": 30,
		"name": "Cultural81",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/Cultural81.png",
			"frame": {
				"x": 65,
				"y": 738,
				"width": 202,
				"height": 258
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "298415814"
	}
]